///////////////////////////////////////////////////////////////////////////////
//                                                                           //
//                          *** Outcar.hpp ***                               //
//                                                                           //
/// Get most useful quantities from OUTCAR that will be used in new runs     //
//                                                                           //
// created November 24, 2018                                                 //
// copyright Christopher N. Singh Binghamton University Physics              //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////

#ifndef Outcar_hpp
#define Outcar_hpp

#include <iostream>
#include <fstream>
#include <string>
#include "nixio/nixio.hpp"

// Needs to be able to search through the file line by line until a match is 
// found. Then process the matching line to get the toten

class Outcar
{
	public:

		Outcar(std::string filename) {initialize(filename);}

		void initialize(std::string filename);

		friend bool operator> (const Outcar& o1, const Outcar& o2);
		friend bool operator>=(const Outcar& o1, const Outcar& o2);
		friend bool operator< (const Outcar& o1, const Outcar& o2);
		friend bool operator<=(const Outcar& o1, const Outcar& o2);

		double free_energy;
		double internal_energy;
		std::string m_filename;
};

#endif /* Outcar_hpp */

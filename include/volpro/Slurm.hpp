///////////////////////////////////////////////////////////////////////////////
//                                                                           //
//                          *** Slurm.hpp ***                                //
//                                                                           //
/// Generate slurm submission script to be placed in run dirs                //
//                                                                           //
// created November 25, 2018                                                 //
// copyright Christopher N. Singh Binghamton University Physics              //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////

#ifndef Slurm_hpp
#define Slurm_hpp

#include <iostream>
#include <string>
#include <fstream>
#include <boost/filesystem.hpp>
#include "nixio/nixio.hpp"

// This class just needs to write a slurm submission script
// the user should be able to supply job name algorithmically while the user
// name can hopefully be pulled from the system. Maybe should first check 
// if user name is valid and if valid go with it or promt for entry

class Slurm
{
	public:

		Slurm();

		/// writes the slurm submit script based on internal state
		void write_to(std::string job_dir);
		
		/// set the name of the actual file
		void set_filename(std::string filename);

		/// get filename that will label batch submission script
		std::string get_filename(void);

		/// set a job name to appear in squeue
		void set_jobname(std::string jobname);

        /// set the slurm partition
        void set_partition(std::string partition);

		/// set number of tasks, this is most important setting for multi-core
		void set_ntasks(std::string ntasks);

		/// set number of nodes, best to be one if possible, reduce MPI comms
		void set_nodes(std::string nodes);

		/// useful for openmp type tasks
		void set_tasks_per_node(std::string tasks_per_node);

		/// set for openmp 
		void set_cpus_per_task(std::string cpus_per_task);
    
		/// set the sockets-per-node slurm parameter
		void set_sockets_per_node(std::string sockets_per_node);

		/// set the cores-per-socket slurm parameter
		void set_cores_per_socket(std::string cores_per_socket);

		/// set the threads-per-core slurm parameter
		void set_threads_per_core(std::string threads_per_core);
		
		/// set mem-per-cpu slurm variable 
		void set_mem_per_cpu(std::string mem_per_cpu);

		/// set mem-per-cpu slurm variable 
		void set_mem(std::string mem);

		/// set slurm exclude parameter 
		void set_exclude(std::string exclude);
		
		/// set mail type... follows slurm version circa version 18 options
		void set_mail_type(std::string mail_type);
		
		/// insert and non-standard string ... could be module commands etc
		void set_user_strings(std::vector<std::string> quote_delimited_strings);
		
		/// choose from std, gam, ncl types 
		void set_vasp_version(std::string vasp_version);

	private:
	   	std::string m_username;
		std::string m_filename;
		std::string m_jobname;

        std::string m_partition;
		std::string m_ntasks;
		std::string m_nodes;
		std::string m_tasks_per_node;
		std::string m_cpus_per_task;
		std::string m_sockets_per_node;
		std::string m_cores_per_socket;
		std::string m_threads_per_core;
		std::string m_mem_per_cpu;
		std::string m_mem;
		std::string m_exclude;
		
		std::string m_mail_type;
		
		std::vector<std::string> m_user_strings;
		std::string m_vasp_version;
};


#endif /* Slurm_hpp */

///////////////////////////////////////////////////////////////////////////////
//                                                                           //
//                           *** Point.hpp ***                               //
//                                                                           //
/// Helper class to deal with points in 3D Euclidean spaces                  //
//                                                                           //
// created November 24, 2018                                                 //
// copyright Christopher N. Singh Binghamton University Physics              //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////

#ifndef Point_hpp
#define Point_hpp

#include <iostream>

class Point
{
	public:

		double x;
		double y;
		double z;

		friend std::ostream& operator<<(std::ostream& os, const Point& point);
		friend std::istream& operator>>(std::istream& is, Point& point);

	private:
};


#endif /* Point_hpp */

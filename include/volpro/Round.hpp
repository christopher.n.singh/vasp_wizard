///////////////////////////////////////////////////////////////////////////////
//                                                                           //
//                           *** Round.hpp ***                               //
//                                                                           //
/// Resonsible for generating Nc POSCARS based on point in the VP curve      //
//                                                                           //
// created November 21, 2018                                                 //
// copyright Christopher N. Singh Binghamton University Physics              //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////

#ifndef Round_hpp
#define Round_hpp 

#include <boost/filesystem.hpp>
#include <boost/progress.hpp>
#include <boost/regex.hpp>
#include <volpro/Poscar.hpp>
#include <volpro/Slurm.hpp>
#include <volpro/Outcar.hpp>
#include <nixio/nixio.hpp>
#include <volpro/Point.hpp>
#include <algorithm>

class Round
{
	public:

		/// round constructor, sets info based on supercell
		Round(std::string intercalant, Poscar& original_supercell);

		/// can pass a slurmfile for more job control
		void generate_posistion_test(std::size_t round_number, Slurm slurmfile);

	private:

		std::size_t m_round_number;
		std::size_t m_total_intercalants;
		std::string m_intercalant;
		Poscar m_original_supercell;

		void append_check_position(std::size_t index);
		void create_new_poscar(std::size_t nth_posistion_to_check);
		std::string job_dir(std::size_t nth_posistion_to_check, std::size_t round_number);
		Point lowest_energy_position(std::size_t round_number);
		void copy_local_vasp_input_files(std::string job_dir);
		void check_existence_of_job_dirs(std::size_t round_number);
		std::string lowest_energy_contcar(std::size_t round_number);
		std::vector<std::string> all_possible_dirs(std::size_t round_number);
		std::vector<std::string> all_valid_dirs(std::size_t round_number);
		std::vector<std::size_t> all_valid_check_positions(std::size_t round_number);
		std::size_t lowest_energy_index(std::size_t round_number);
};

#endif /* Round_hpp */

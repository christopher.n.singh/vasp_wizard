///////////////////////////////////////////////////////////////////////////////
//                                                                           //
//                          *** Poscar.hpp ***                               //
//                                                                           //
/// Base class responsible for reading and storing a POSCAR                  //
//                                                                           //
// created November 20, 2018                                                 //
// copyright Christopher N. Singh Binghamton University Physics              //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////

#ifndef Poscar_hpp
#define Poscar_hpp

#include <string>
#include <cassert>
#include <iostream>
#include <iomanip>
#include <cmath>
#include <cstdlib>
#include <vector>
#include <numeric>
#include <algorithm>
#include "nixio/nixio.hpp"
#include "volpro/Point.hpp"

class Poscar
{
	public:

		Poscar() { is_initialized = false; }

		/// Primary initialization from a file
		void initialize(std::string filename);

		/// Will reset the member variables to initializd state
		void reset();

		/// Remove atom of specific element specified by its index, mutates state
		void remove_atom(std::string element, std::size_t index);

		/// Insert an atom into the poscar at p, mutates state
		void insert_atom(std::string element, const Point &p);

		/// write current state of poscar to a file
		void write(std::string filename) const;

		/// print the state of the poscar to stdout
		void print(void) const;

		/// return the posisiton of element at index 
		Point get_posistion(std::string element, std::size_t index);

		/// return the number of atoms of type element
		std::size_t get_quantity(std::string element);

	private:

		bool is_initialized;
		double scale_factor;
		Point a_lattice_vector;
		Point b_lattice_vector;
		Point c_lattice_vector;

		std::string initialization_file;
		std::string header;
		std::string coordinate_system;
		std::vector<std::string> elements;
		std::vector<std::size_t> quantities;
		std::vector<Point> positions;

		bool element_exists(std::string element);
		bool has_valid_index(std::string element, std::size_t index);
		void decrement_quantity(std::string element);
		void increment_quantity(std::string element);
		void erase_position(std::string element, std::size_t index);
		void insert_position(std::string element, const Point &p);
		std::size_t element_index(std::string element);
};






#endif /* Poscar_hpp */

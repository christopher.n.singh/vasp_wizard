///////////////////////////////////////////////////////////////////////////////
//                                                                           //
//                          *** Eigenval.hpp ***                             //
//                                                                           //
/// Responsible for reading in and storing the information in EIGENVAL       //
//                                                                           //
// created November 09, 2018                                                 //
// copyright Christopher N. Singh Binghamton University Physics              //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////

#ifndef Eigenval_hpp
#define Eigenval_hpp

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <vector>
#include "nixio/nixio.hpp"
#include "boost/filesystem.hpp"

class Eigenval
{
	public:

		Eigenval(){}
	
		/// Generate a file with the k1, k2 pairs and corresponding gap	
		void write_gapcar(void);
		void write_bandcar(void);
		void write_gapcar(int argc, char *argv[]);

	private:

		int ions;
		int pair_correlation_loops;
		int ispin;
		int electrons;
		int kpoints;
		int bands;	

		double volume_of_cell;
		double a_lattice_param;
		double b_lattice_param;
		double c_lattice_param;
		double yet_unknown_parameter;
		double temperature;

		std::string car;
		std::string header;
		
		int smallest_gap_k1;
		int smallest_gap_k2;
		int largest_gap_k1;
		int largest_gap_k2;

		std::vector<double> kvecs;
		std::vector<double> value;
		std::vector<double> gaps;
		
		double occupation(int kpoint, int band);
		double energy(int kpoint, int band);
		double conduction_band_minimum(int kpoint);
		double valence_band_maximum(int kpoint);
		double gap(int k1, int k2);
		double smallest_gap(void);
		double largest_gap(void);

		void initialize(std::string file);
	 	void calculate_momentum_dependent_gap(void);
		void write_momentum_dependent_gap(std::string file);
		void find_largest_gap_k1k2(void);
		void find_smallest_gap_k1k2(void);	
		void print(void);
};


#endif /* Eigenval_hpp */

///////////////////////////////////////////////////////////////////////////////
//                                                                           //
//                          *** Doscar.hpp ***                               //
//                                                                           //
// created December 05, 2019                                                 //
// copyright Christopher N. Singh Binghamton University Physics              //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////

#ifndef Doscar_hpp
#define Doscar_hpp

#include <vector>
#include <iterator>
#include "nixio/nixio.hpp"

class Doscar
{
	public:

		Doscar(std::string filename);

		void print(void) const;
		void write_total_dos(void) const;
		void write_decom_dos(void) const;
		void sum_over_atoms(void);
		void set_relative_fermi(void);

		int partial_dos_switch; 
		double volume; 
		double a_lattice_vector;
		double b_lattice_vector;
		double c_lattice_vector;
		double potim, tebeg;
		double emin, emax, efermi;
		std::string system_name;
		std::size_t nions, nedos; 

		std::vector<double> energies;
		std::vector<double> total; 
		std::vector<double> integrated; 
		std::vector<double> s, px, py, pz, dxy, dyz, dz2, dxz, dx2;  
};






#endif /* Doscar_hpp */

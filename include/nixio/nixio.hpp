///////////////////////////////////////////////////////////////////////////////
//                                                                           //
//                           *** nixio.hpp ***                               //
//                                                                           //
/// Conversion of strings and string streams into more useful types          //
//                                                                           //
// created November 24, 2018                                                 //
// copyright Christopher N. Singh Binghamton University Physics              //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////

#ifndef nixio_hpp
#define nixio_hpp

#include <iostream>
#include <fstream>
#include <sstream>

/// Pull a stringstream directly from a file by line
void get_streamline(std::ifstream &file, std::istringstream &streamline);

/// Form a stringstream given the corresponding string
void form_streamline(std::string &line, std::istringstream &streamline);

/// Vasp Wizard ASCII art
void welcome_message_v1(void);
void welcome_message_v2(void);

int get_program_choice(void);

void check_input_files(void);

void open_ascii_escape(std::string color);
void close_ascii_escape();

void sanity_warning(std::string best_poscar);
	
#endif /* nixio_hpp */

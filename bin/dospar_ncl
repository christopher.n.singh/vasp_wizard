#!/bin/bash 

# This file is part of vasp_wizard - parses DOSCAR for vasp_ncl
# with LORBIT = 11 and ISPIN = 1

# Copyright 2020 Christopher N. Singh - Los Alamos National Laboratory

# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

[ ! -f DOSCAR ] && echo NO DOSCAR FOUND! && exit
[ ! -f POSCAR ] && echo NO POSCAR FOUND! && exit 
[ ! -f INCAR ] && echo NO INCAR FOUND! && exit

EF=$(awk 'NR==6 {print $3}' DOSCAR)
NEDOS=$(grep NEDOS INCAR | awk '{print $3}')
NIONS=$(awk 'NR==1 {print $1}' DOSCAR)
NTYPE=$(awk 'NR==6 {print NF}' POSCAR)
VOLUME=$(awk 'NR==2 {print $1}' DOSCAR)
IONS=($(awk 'NR==6 {print}' POSCAR))
COUNTS=($(awk 'NR==7 {print}' POSCAR))



[ -f TOTAL-DOS ] && rm TOTAL-DOS

echo "#  E-Ef      DOS      IDOS" > TOTAL-DOS

awk -v ef=$EF -v nedos=$NEDOS -v vol=$VOLUME \
	'NR==7,NR==nedos+6 {print $1-ef, $2/vol, $3}' \
	DOSCAR >> TOTAL-DOS

echo TOTAL-DOS written

[ -f DECOM-DOS ] && rm DECOM-DOS

awk -v ef=$EF -v nedos=$NEDOS \
	'NR > 6+nedos {print $0}' \
	DOSCAR > tmp

awk 'NF==37 {print $0}' tmp > a
awk 'NF==28 {print $0}' tmp > b 
paste a b > c

awk -v ef=$EF \
	'{
	$1=$1-ef; print $1, $2, $6+$10+$14, 
	$18+$22+$26+$30+$34,
	$38+$42+$46+$50+$54+$58+$62
	 }' \
	c > tmp

split --numeric=1 -l 20001 tmp ldos-

# loop over LDOS files and sum over same species
for idx in ${!COUNTS[@]}
do
	tmp=0
	for i in ${COUNTS[@]:0:$idx}
	do
		let tmp+=$i
	done

	DIR=${IONS[$idx]}

	mkdir -p $DIR

	for n in $(seq -f "%02g" $(($tmp+1)) $((${COUNTS[$idx]}+$tmp)))
	do
		cp ldos-$n $DIR
	done

	cd $DIR
	for L in {2..5}
	do
		awk -v orb=$L \
			'{sum[$1]+=$orb} END {for(i in sum) print i, sum[i]}' \
			ldos-* | sort -g > ion-summed-pdos-$L
	done

	mv ion-summed-pdos-2 ion-summed-pdos-s
	mv ion-summed-pdos-3 ion-summed-pdos-p
	mv ion-summed-pdos-4 ion-summed-pdos-d
	mv ion-summed-pdos-5 ion-summed-pdos-f

	rm ldos-*
	cd ..	
done
rm a b c tmp ldos-*


mkdir s p d f

for dir in {s,p,d,f}
do
	for ion in ${IONS[@]}
	do
		cp $ion/ion-summed-pdos-$dir $dir/$ion
	done

	cd $dir
	awk '{sum[$1]+=$2} END {for(i in sum) print i, sum[i]}' \
		* | sort -g > pdos
	cd ..
done

paste s/pdos p/pdos d/pdos f/pdos > pdos

awk '{print $1,$2,$4,$6,$8}' pdos > DECOM-DOS

[ -d LDOS ] && rm -rf LDOS 

mkdir LDOS

for ion in ${IONS[@]}
do
	mv $ion LDOS	
done

rm -rf s p f d pdos 

echo DECOM-DOS written

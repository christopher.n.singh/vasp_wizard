# Vasp Wizard

This is a small collection of C++ objects to help with the generation of input 
files for VASP runs. 

Often times in doping studies, or in battery simulations it is helpful to be 
able to algorithmically manipulate the crystal structures, where the amount
of time to do it by hand is far too much. It is also difficult to get it right 
in those circumstances and errors can quickly multiply.

As of 2018-11-26, this project is still raw with no API. 

## Building

This has been checked to compile with g++ and clang++ 
on OSX 10.13.6 and CentOS 7.4.1708, and with gcc/icpc on CentOS 7.4.1708.

To use it, clone it and build it. For everyone the cloning is the same

- `git clone https://gitlab.com/christopher.n.singh/vasp_wizard.git`
- `cd vasp_wizard`

If you do not have the boost libraries, these need to be installed. Depending
on your platform, the installation procedure may be different, but most systems
have the boost libraries packaged by a package manager. 

On OSX (with homebrew)
- `brew install boost`

On Linux (Red-Hat)
- `sudo yum install boost`

If you use cmake, and do no want any fine control over the compilation, try
these steps to compile.
 
- `cd build && cmake ../etc`
- `make`

If you want to have more control, know what compiler you would like to use, aka
if you would prefer to use clang over gcc, you can try the hand generated 
Makefile sitting in the project root. In that case the steps would try

- `make docs`
- `make`

## Quickstart

Since there is no API yet, edit the main method.



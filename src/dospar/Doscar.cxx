///////////////////////////////////////////////////////////////////////////////
//                                                                           //
//                          *** Doscar.cpp ***                               //
//                                                                           //
// created December 5, 2019                                                  //
// copyright Christopher N. Singh Binghamton University Physics              //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////

#include "dospar/Doscar.hpp"
#include "volpro/Parser.hpp"
#include <cassert>

Doscar::Doscar(std::string filename) 
{
	std::ifstream file(filename.c_str());
	
	// Working variable, stringstream of each line in file 
	std::istringstream streamline;

	if (file.is_open()) {

		// Pull out number of ions and decomp flag;
		get_streamline(file, streamline);
		streamline >> nions;
		streamline >> nions;
		streamline >> partial_dos_switch;

		// Pull out volume and lattice vectors
		get_streamline(file, streamline);
		streamline >> volume;
		streamline >> a_lattice_vector;
		streamline >> b_lattice_vector;
		streamline >> c_lattice_vector;
		streamline >> potim;
		
		// Pull tebeg
		get_streamline(file, streamline);
		streamline >> tebeg;
		
		// Pull out word 'CAR'
		get_streamline(file, streamline);

		// Pull out system name
		get_streamline(file, streamline);
		streamline >> system_name;
		
		get_streamline(file, streamline);

		auto size = [](std::string ss) -> std::size_t
		{
			std::istringstream iss(ss);
			typedef std::istream_iterator<std::string> itr;
			std::vector<std::string> line(itr{iss}, itr());	

			return line.size();
		};

		if (size(streamline.str()) == 5)
		{
			streamline >> emax >> emin >> nedos >> efermi;
		}
		else if (size(streamline.str()) == 4)
		{
			/*
			open_ascii_escape("yellow");
			std::cout << "\n WARNING: NEDOS not found in DOSCAR..."
				<< "looking for INCAR but if not available will have quit\n\n";
			close_ascii_escape();
			*/

			Parser incar("INCAR");
			nedos = static_cast<std::size_t>(std::atoi(incar.value_of_key("NEDOS").c_str()));
			streamline >> emax >> emin >> efermi;
		}
		else 
		{
			open_ascii_escape("red");
			std::cout << "\n ERROR!!! COULD NOT DETERMINE NEDOS\n\n";
			close_ascii_escape();
			std::exit(-1);
		}


		energies.resize(nedos);
		total.resize(nedos);
		integrated.resize(nedos);
		s.resize(nedos * nions);
		px.resize(nedos * nions);
		py.resize(nedos * nions);
		pz.resize(nedos * nions);
		dxy.resize(nedos * nions);
		dyz.resize(nedos * nions);
		dz2.resize(nedos * nions);
		dxz.resize(nedos * nions);
		dx2.resize(nedos * nions);

		// first pull out total dos
		for (std::size_t i = 0; i < nedos; ++i)
		{
			get_streamline(file, streamline);
			streamline >> energies[i] >> total[i] >> integrated[i];
		}


		/* loop over each atom projected lm-series, 
		 * rememering to pull out that useless line seperating atoms
		 * also need to shift vector index by nedos*ion_number 
		 */
		std::size_t idx;
		for (std::size_t ion = 0; ion < nions; ++ion)
		{
			get_streamline(file, streamline);
			for (std::size_t i = 0; i < nedos; ++i)
			{
				get_streamline(file, streamline); 
				
				idx = i + ion*nedos;
				streamline >> energies[i] >> s[idx]
					>> px[idx] >> py[idx] >> pz[idx]
					>> dxy[idx] >> dyz[idx] >> dz2[idx] >> dxz[idx] >> dx2[idx]; 
			}
		}

	} 
	else 
	{
		open_ascii_escape("red");
		std::cout << "\n ERROR!!! DOSCAR NOT FOUND\n\n";
		close_ascii_escape();
		std::exit(-1);
	}
}

void Doscar::write_total_dos(void) const 
{
	std::ofstream file("TOTAL-DOS");

	if (file.is_open())
	{	
		file << "# E-Ef \t total \t integrated \n";
		file << std::scientific;
		for (std::size_t i = 0; i < nedos; ++i)
		{
			file << energies[i] << '\t' 
				 << total[i] << '\t' 
				 << integrated[i] << '\n';
		}
		std::cout << "TOTAL-DOS written\n";
	}
	else 
	{
		open_ascii_escape("red");
		std::cout << "\n ERROR!!! COULD NOT WRITE TOTAL-DOS\n\n";
		close_ascii_escape();
		std::exit(-1);
	}
}

void Doscar::write_decom_dos(void) const
{
	std::ofstream file("DECOM-DOS");

	if (file.is_open())
	{	
		file << "# E-Ef \t s \t px \t py \t pz \t"
			 << "dxy \t dyz \t dz2 \t dxz \t dx2 \n";

		file << std::scientific;
		
		for (std::size_t i = 0; i < nedos; ++i)
		{
			file << energies[i]    << '\t' << s[i]
				 << '\t' << px[i]  << '\t' << py[i]  << '\t' << pz[i]
				 << '\t' << dxy[i] << '\t' << dyz[i] << '\t' << dz2[i] 
				 << '\t' << dxz[i] << '\t' << dx2[i] << '\n';
		}
		std::cout << "DECOM-DOS written\n";

	}
	else 
	{
		open_ascii_escape("red");
		std::cout << "\n ERROR!!! COULD NOT WRITE TOTAL-DOS\n\n";
		close_ascii_escape();
		std::exit(-1);
	}
}

void Doscar::print(void) const
{
	std::cout << nions << '\t' << nions << '\t' << partial_dos_switch << '\n'
		<< volume << '\t' 
		<< a_lattice_vector << '\t'
		<< b_lattice_vector << '\t'
		<< c_lattice_vector << '\t'
		<< potim << '\n'
		<< tebeg << '\n'
		<< "CAR" << '\n'
		<< system_name << '\n'
		<< emax << '\t' << emin << '\t' << nedos << '\t' << efermi << '\n';
}

void Doscar::sum_over_atoms(void)
{
	std::vector<double> s_sum(nedos);
	std::vector<double> px_sum(nedos);
	std::vector<double> py_sum(nedos);
	std::vector<double> pz_sum(nedos);
	std::vector<double> dxy_sum(nedos);
	std::vector<double> dyz_sum(nedos);
	std::vector<double> dz2_sum(nedos);
	std::vector<double> dxz_sum(nedos);
	std::vector<double> dx2_sum(nedos);

	for (std::size_t ion = 0; ion < nions; ++ion)
	{
		for (std::size_t nrg = 0; nrg < nedos; ++nrg)
		{
			s_sum[nrg] += s[nrg + nedos*ion];
			px_sum[nrg] += px[nrg + nedos*ion];
			py_sum[nrg] += py[nrg + nedos*ion];
			pz_sum[nrg] += pz[nrg + nedos*ion];
			dxy_sum[nrg] += dxy[nrg + nedos*ion];
			dyz_sum[nrg] += dyz[nrg + nedos*ion];
			dz2_sum[nrg] += dz2[nrg + nedos*ion];
			dxz_sum[nrg] += dxz[nrg + nedos*ion];
			dx2_sum[nrg] += dx2[nrg + nedos*ion];
		}
	}

	s = s_sum;
	px = px_sum; 
	py = py_sum;
	pz = pz_sum;
	dxy = dxy_sum;
	dyz = dyz_sum;
	dz2 = dz2_sum;
	dxz = dxz_sum;
	dx2 = dx2_sum;
}


void Doscar::set_relative_fermi(void)
{
	for (auto& nrg : energies) nrg -= efermi;
}


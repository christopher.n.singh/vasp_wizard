///////////////////////////////////////////////////////////////////////////////
//                                                                           //
//                           *** main.cpp ***                                //
//                                                                           //
// created November 20, 2018                                                 //
// copyright Christopher N. Singh Binghamton University Physics              //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////

#include "dospar/Doscar.hpp"
#include <nixio/nixio.hpp>

int main()
{
	welcome_message_v2();

	Doscar doscar("DOSCAR");	
	doscar.sum_over_atoms(); 
	doscar.set_relative_fermi();
	doscar.write_total_dos();
	doscar.write_decom_dos();
	 
	return 0;
}


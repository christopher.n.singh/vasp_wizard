///////////////////////////////////////////////////////////////////////////////
//                                                                           //
//                           *** nixio.cpp ***                               //
//                                                                           //
/// Need set of basic nix sys commands bc std::filesystem not portably yet   //
//                                                                           //
// created November 24, 2018                                                 //
// copyright Christopher N. Singh Binghamton University Physics              //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////

#include "nixio/nixio.hpp"

void get_streamline(std::ifstream &file, std::istringstream &streamline)
{
	std::string line;

	std::getline(file, line);
	streamline.str(std::string());
	streamline.clear();
	streamline.str(line);
}

void form_streamline(std::string &line, std::istringstream &streamline)
{
	streamline.str(std::string());
	streamline.clear();
	streamline.str(line);
}

void welcome_message_v1(void)
{
	std::cout << "\n";
	open_ascii_escape("purple");

	std::cout << " ___      ___ ________  ________  ________        ___       ";
	std::cout << "__   ___  ________  ________  ________  ________    \n";

	std::cout << "|\\  \\    /  /|\\   __  \\|\\   ____\\|\\   __  \\      ";
	std::cout << "|\\  \\     |\\  \\|\\  \\|\\_____  \\|\\   __  \\|\\   __  ";
	std::cout << "\\|\\   ___ \\   \n";

	std::cout << "\\ \\  \\  /  / | \\  \\|\\  \\ \\  \\___|\\ \\  \\|\\  ";
	std::cout << "\\     \\ \\  \\    \\ \\  \\ \\  \\\\|___/  /\\ \\  \\|\\  ";
	std::cout << "\\ \\  \\|\\  \\ \\  \\_|\\ \\   \n";

	std::cout << " \\ \\  \\/  / / \\ \\   __  \\ \\_____  \\ \\   ____\\     ";
	std::cout << "\\ \\  \\  __\\ \\  \\ \\  \\   /  / /\\ \\   __  \\ \\   _ ";
	std::cout << " _\\ \\  \\ \\\\ \\  \n";

	std::cout << "  \\ \\    / /   \\ \\  \\ \\  \\|____|\\  \\ \\  \\___|    ";
	std::cout << "  \\ \\  \\|\\__\\_\\  \\ \\  \\ /  /_/__\\ \\  \\ \\  \\ \\";
	std::cout << "  \\\\  \\\\ \\  \\_\\\\ \\ \n";

	std::cout << "   \\ \\__/ /     \\ \\__\\ \\__\\____\\_\\  \\ \\__\\      ";
	std::cout << "    \\ \\____________\\ \\__\\\\________\\ \\__\\ \\__\\ ";
	std::cout << "\\__\\\\ _\\\\ \\_______\\\n";

	std::cout << "    \\|__|/       \\|__|\\|__|\\_________\\|__|           ";
	std::cout << "\\|____________|\\|__|\\|_______|\\|__|\\|__|\\|__|\\|__|\\|";
	std::cout << "_______|\n";

	std::cout << "\n";

	std::cout << "                                                  ";
	std::cout << "                               © Christopher N. Singh \n";
	std::cout << "                                                  ";
	std::cout << "                            created November 24, 2018\n";

	close_ascii_escape();
	std::cout << "\n\n";

	std::string user = getenv("USER");

	std::cout << " Welcome " << user << std::endl;
}

int get_program_choice(void) 
{
	int choice = -1;

	std::cout << " Here is what I can do for you\n\n";
	std::cout << "\t(0) Generate a round for a voltage profile\n";
	std::cout << "\t(1) Calculate the momentum dependent gap\n";

	std::cout << "\n";

	while (true) {
		std::cout << " Choose an action from the list\n";
		std::cin >> choice;

		if (std::cin.fail() || choice > 1 || choice < 0) {
			std::cin.clear();
			std::cin.ignore(32767,'\n');
			std::cout << " Seems like you entered invalid input!\n"; 
		} else { 
			return choice;
		}
	}
}

void check_input_files(void)
{
	std::cout << " Running Peter Larrson's vaspcheck...\n";
	std::system("vaspcheck");
	std::cout << " If you see no warnings your INCAR is OK\n\n";
	std::cout << " Running vset to suggest parallel params...\n\n";
	std::system("vset");
	std::cout << '\n';
	std::cout << " You might have to adjust DOCKET for SLURM settings\n\n";
}

void open_ascii_escape(std::string color)
{
	if (color == "red")    std::cout << "\033[1;31m";
	if (color == "green")  std::cout << "\033[1;32m";
	if (color == "yellow") std::cout << "\033[1;33m";
	if (color == "blue")   std::cout << "\033[1;34m";
	if (color == "purple") std::cout << "\033[1;35m";
	if (color == "cyan")   std::cout << "\033[1;36m";
	if (color == "white")  std::cout << "\033[1;37m";
}

void close_ascii_escape(void)
{
	std::cout << "\033[0m";
}

void sanity_warning(std::string best_poscar)
{
	std::cout << "\n";	
	open_ascii_escape("purple");
	std::cout << " Dear " << std::getenv("USER") << ",\n\n";
	std::cout << " The best position for that round came from ";
	open_ascii_escape("yellow");
   	std::cout << best_poscar << "\n";
	close_ascii_escape();
	open_ascii_escape("purple");
	std::cout << " However, that statement is not meaningful if you see jobs\n";
	std::cout << " that did not converge. I am going to continue, but it is\n";
	std::cout << " up to you to make sure that your energies are physical\n\n";
	std::cout << "                                   Sincerely, The Wizard\n\n";
	close_ascii_escape();
}

void welcome_message_v2()
{
	std::cout << "\n";
	open_ascii_escape("purple");

	std::cout << "                                 _                  _ \n";
	std::cout << "                                (_)                | |\n";
	std::cout << "__   ____ _ ___ _ __   __      ___ ______ _ _ __ __| |\n";
	std::cout << "\\ \\ / / _` / __| '_ \\  \\ \\ /\\ / / |_  / _` | '__/ _` |\n";
	std::cout << " \\ V / (_| \\__ \\ |_) |  \\ V  V /| |/ / (_| | | | (_| |\n";
	std::cout << "  \\_/ \\__,_|___/ .__/    \\_/\\_/ |_/___\\__,_|_|  \\__,_|\n";
	std::cout << "               | |                                    \n";
	std::cout << "               |_|                                    \n";

	std::cout << "                ";
	std::cout << "                © Christopher N. Singh \n";
	std::cout << "                ";
	std::cout << "             created November 24, 2018 \n";

	close_ascii_escape();
	std::cout << '\n' << std::endl;

	//std::string user = getenv("USER");
	//std::cout << " Welcome " << user << std::endl;
}

///////////////////////////////////////////////////////////////////////////////
//                                                                           //
//                           *** main.cpp ***                                //
//                                                                           //
// created November 20, 2018                                                 //
// copyright Christopher N. Singh Binghamton University Physics              //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////

#include "common/Eigenval.hpp"
#include "nixio/nixio.hpp"

int main()
{
	welcome_message_v2();

	Eigenval eigenval; 
	eigenval.write_gapcar();

	return 0;
}


///////////////////////////////////////////////////////////////////////////////
//                                                                           //
//                           *** Slurm.cpp ***                               //
//                                                                           //
/// Simple helper class that can be passed around to generate batch file     //
//                                                                           //
// created November 24, 2018                                                 //
// copyright Christopher N. Singh Binghamton University Physics              //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////

#include "volpro/Slurm.hpp"

Slurm::Slurm() 
{
	m_username = getenv("USER");
	m_filename = "runvasp";
	m_jobname = "";
	
    m_partition = "";
	m_ntasks = "";
	m_nodes = "";
	m_tasks_per_node = "";
	m_cpus_per_task = "";
	m_sockets_per_node = "";
	m_cores_per_socket = "";
	m_threads_per_core = "";
	m_mem_per_cpu = "";
	m_mem = "";
	m_exclude = "";
	m_mail_type = "";

	m_vasp_version = "";
}

void Slurm::write_to(std::string job_dir)
{

	std::ofstream file(job_dir + m_filename);
	
	if (file.is_open()) {
		file << "#!/bin/bash\n\n";

		if (m_jobname.size() != 0) {
			file << "#SBATCH --job-name=";
			file << m_jobname << "\n";
		}

        if (m_partition.size() != 0) {
            file << "#SBATCH --partition=";
            file << m_partition << "\n";
        }
        
		if (m_ntasks.size() != 0) {
			file << "#SBATCH --ntasks=";
			file << m_ntasks << "\n";
		}

		if (m_nodes.size() != 0) {
			file << "#SBATCH --nodes=";
			file << m_nodes << "\n";
		}

		if (m_tasks_per_node.size() != 0) {
			file << "#SBATCH --tasks-per-node=";
			file << m_tasks_per_node << "\n";
		}

		if (m_cpus_per_task.size() != 0) {
			file << "#SBATCH --cpus-per-task=";
			file << m_cpus_per_task << "\n";
		}
		
		if (m_sockets_per_node.size() != 0) {
			file << "#SBATCH --sockets-per-node=";
			file << m_sockets_per_node << "\n";
		}

		if (m_cores_per_socket.size() != 0) {
			file << "#SBATCH --cores-per-socket=";
			file << m_cores_per_socket << "\n";
		}

		if (m_threads_per_core.size() != 0) {
			file << "#SBATCH --threads-per-core=";
			file << m_threads_per_core << "\n";
		}
		
		if (m_mem_per_cpu.size() != 0) {
			file << "#SBATCH --mem-per-cpu=";
			file << m_mem_per_cpu << "\n";
		}
		
		if (m_mem.size() != 0) {
			file << "#SBATCH --mem=";
			file << m_mem << "\n";
		}

		if (m_exclude.size() != 0) {
			file << "#SBATCH --exclude=";
			file << m_exclude << "\n";
		}
		
		if (m_mail_type.size() != 0) {
			file << "#SBATCH --mail-type=";
			file << m_mail_type << "\n";
			file << "#SBATCH --mail-user=";
			file << m_username << "@binghamton.edu\n";
		}
		
		if (m_user_strings.size() != 0) 
		{
			file << "\n";
			for (const auto &i : m_user_strings) file << i << '\n';
		}

		file << "\nmpirun vasp_";
		if (m_vasp_version.size() != 0) 
			file << m_vasp_version << "\n";
		else 
			file << "std" << "\n";

		boost::filesystem::path p(job_dir + m_filename);
		boost::filesystem::permissions(p, boost::filesystem::owner_all);

	} else {
		open_ascii_escape("yellow");
		std::cout << "\n WARNING!!! COULD NOT GENERATE SLURM FILE\n\n";
		close_ascii_escape();
	}	

}

void Slurm::set_filename(std::string filename)
{
	m_filename = filename;
}

std::string Slurm::get_filename(void)
{
	return m_filename;
}

void Slurm::set_jobname(std::string jobname) 
{
   	m_jobname = jobname; 
}

void Slurm::set_partition(std::string partition)
{
    m_partition = partition;
}

void Slurm::set_vasp_version(std::string vasp_version)
{
	m_vasp_version = vasp_version;
}

void Slurm::set_mail_type(std::string mail_type)
{
	m_mail_type = mail_type;
}

void Slurm::set_ntasks(std::string ntasks) 
{ 
	m_ntasks = ntasks; 
}

void Slurm::set_nodes(std::string nodes) 
{ 
	m_nodes = nodes; 
}

void Slurm::set_tasks_per_node(std::string tasks_per_node) 
{ 
	m_tasks_per_node = tasks_per_node; 
}

void Slurm::set_cpus_per_task(std::string cpus_per_task) 
{ 
	m_cpus_per_task = cpus_per_task;
}

void Slurm::set_user_strings(std::vector<std::string> quote_delimited_strings)
{
	m_user_strings = quote_delimited_strings;
}

void Slurm::set_mem_per_cpu(std::string mem_per_cpu)
{
	m_mem_per_cpu = mem_per_cpu;
}

void Slurm::set_mem(std::string mem)
{
	m_mem = mem;
}

void Slurm::set_exclude(std::string exclude)
{
	m_exclude = exclude;
}

void Slurm::set_sockets_per_node(std::string sockets_per_node)
{
	m_sockets_per_node = sockets_per_node;
}

void Slurm::set_cores_per_socket(std::string cores_per_socket)
{
	m_cores_per_socket = cores_per_socket;
}

void Slurm::set_threads_per_core(std::string threads_per_core)
{
	m_threads_per_core = threads_per_core;
}
















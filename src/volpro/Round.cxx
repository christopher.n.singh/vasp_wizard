///////////////////////////////////////////////////////////////////////////////
//                                                                           //
//                           *** Round.cpp ***                               //
//                                                                           //
// Class resonsible for generating Nc POSCARS based on the point in the      //
// voltage profiling curve                                                   //
//                                                                           //
// created November 21, 2018                                                 //
// copyright Christopher N. Singh Binghamton University Physics              //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////

#include "volpro/Round.hpp"

Round::Round(std::string intercalant, Poscar& original_supercell)
{
	m_round_number = 0;
	m_intercalant = intercalant;
	m_original_supercell = original_supercell;
	m_total_intercalants = m_original_supercell.get_quantity(intercalant);
}

void Round::generate_posistion_test(std::size_t round_number, Slurm slurmfile)
{
	m_round_number = round_number;

	std::cout << "Lowest energy CONTCARS thus far...\n";
	for (std::size_t i = 0; i < round_number; ++i) check_existence_of_job_dirs(i);

	auto script_index = [round_number](void) -> std::string
	{
		if (round_number < 10) return "0" + std::to_string(round_number);
		else return std::to_string(round_number);
	};
	
	std::string scr = "submit_round" + script_index() + ".sh";
	std::ofstream script(scr.c_str());
	script << "#!/bin/bash\n\n";

	if (round_number == 0)
	{
		boost::progress_display progress(all_valid_check_positions(round_number).size());
		for (auto check_position : all_valid_check_positions(round_number))
		{
			//std::cout << "Creating job directory ";
			//std::cout << job_dir(check_position, round_number) << '\n';
			boost::filesystem::create_directory(job_dir(check_position, round_number));

			create_new_poscar(check_position);
			m_original_supercell.write(job_dir(check_position, round_number) + "POSCAR");
			m_original_supercell.reset();

			copy_local_vasp_input_files(job_dir(check_position, round_number));
			slurmfile.set_jobname(job_dir(check_position, round_number));
			slurmfile.write_to(job_dir(check_position, round_number));
			
			// add a line to the round submit script to submit job
			script << "cd " << job_dir(check_position, round_number) << "\n";
			script << "sbatch ";
			script << slurmfile.get_filename() << '\n';
			script << "cd ../\n\n";

			open_ascii_escape("cyan");
			++progress;
			close_ascii_escape();
		}
	}
	else
	{
		boost::progress_display progress(all_valid_check_positions(round_number - 1).size());
		for (auto check_position : all_valid_check_positions(round_number - 1))
		{
			if (check_position == lowest_energy_index(round_number - 1)) 
			{
				open_ascii_escape("cyan");
				++progress;
				close_ascii_escape();
				continue;
			}

			//std::cout << "Creating job directory ";
			//std::cout << job_dir(check_position, round_number) << '\n';
			
			boost::filesystem::create_directory(job_dir(check_position, round_number));

			create_new_poscar(check_position);
			m_original_supercell.write(job_dir(check_position, round_number) + "POSCAR");
			m_original_supercell.reset();

			copy_local_vasp_input_files(job_dir(check_position, round_number));
			slurmfile.set_jobname(job_dir(check_position, round_number));
			slurmfile.write_to(job_dir(check_position, round_number));
			
			// add a line to the round submit script to submit job
			script << "cd " << job_dir(check_position, round_number) << "\n";
			script << "sbatch ";
			script << slurmfile.get_filename() << '\n';
			script << "cd ../\n\n";

			open_ascii_escape("cyan");
			++progress;
			close_ascii_escape();
		}
	}

	boost::filesystem::path p(scr);
	boost::filesystem::permissions(p, boost::filesystem::owner_all);
}

void Round::append_check_position(std::size_t index)
{
	Point keep_posistion = 
		m_original_supercell.get_posistion(m_intercalant, index);

	m_original_supercell.insert_atom(m_intercalant, keep_posistion);
}

void Round::create_new_poscar(std::size_t nth_posistion_to_check)
{
	for (std::size_t i = 0; i < m_round_number; ++i) 
	{
		m_original_supercell.insert_atom(m_intercalant, lowest_energy_position(i));
	}
	
	append_check_position(nth_posistion_to_check);
	
	for (std::size_t i = 0; i < m_total_intercalants; ++i) 
	{
		m_original_supercell.remove_atom(m_intercalant, 0);
	}
}

Point Round::lowest_energy_position(std::size_t round_number)
{
	std::size_t d = lowest_energy_index(round_number);
	
	return m_original_supercell.get_posistion(m_intercalant, d);
}

std::string Round::lowest_energy_contcar(std::size_t round_number)
{
	std::size_t d = lowest_energy_index(round_number);	
	
	return job_dir(d, round_number) + "CONTCAR";
}

std::size_t Round::lowest_energy_index(std::size_t round_number)
{
	std::vector<Outcar> outcars;
	std::vector<std::size_t> avcp = all_valid_check_positions(round_number);

	for (auto check_pos : avcp) 
		outcars.push_back( Outcar(job_dir(check_pos, round_number) + "OUTCAR") );

	auto m = std::min_element(outcars.begin(), outcars.end());
	long d = std::distance(outcars.begin(), m);

	std::size_t ind = static_cast<std::size_t>(d);

	return avcp[ind];
}

void Round::check_existence_of_job_dirs(std::size_t round_number)
{
	bool dir_exists;
	bool contcar_exists;
	bool outcar_exists;

	for (auto check_pos : all_valid_check_positions(round_number)) 
	{
		dir_exists = boost::filesystem::exists(job_dir(check_pos, round_number));
		contcar_exists = boost::filesystem::exists(job_dir(check_pos, round_number) + "CONTCAR");
		outcar_exists = boost::filesystem::exists(job_dir(check_pos, round_number) + "OUTCAR");

		if (dir_exists) 
		{
			if (contcar_exists && outcar_exists) 
			{
				//open_ascii_escape("green");
				//std::cout << " [OK] \n";
				//close_ascii_escape();
			} 
			else if (!outcar_exists) 
			{
				std::cout << "Checking job directory " <<  job_dir(check_pos, round_number) << " ...";
				open_ascii_escape("red");
				std::cout << " [ERROR!!! CANNOT FIND OUTCAR] \n";
				close_ascii_escape();
				std::exit(-1);
			} 
			else if (!contcar_exists) 
			{
				std::cout << "Checking job directory " <<  job_dir(check_pos, round_number) << " ...";
				open_ascii_escape("red");
				std::cout << " [ERROR!!! CANNOT FIND CONTCAR] \n";
				close_ascii_escape();
				std::exit(-1);
			} 
			else 
			{
				std::cout << "Checking job directory " <<  job_dir(check_pos, round_number) << " ...";
				open_ascii_escape("red");
				std::cout << " [ERROR!!! CANNOT FIND OUTCAR AND CONTCAR] \n";
				close_ascii_escape();
				std::exit(-1);
			}

		} else {
			open_ascii_escape("red");
			std::cout << " \n[ERROR!!! CANNOT FIND " << job_dir(check_pos, round_number) << "]\n\n";
			close_ascii_escape();
			exit(-1);
		}
	}

	open_ascii_escape("yellow");
	std::cout << lowest_energy_contcar(round_number) << '\n';
	close_ascii_escape();

	//sanity_warning(lowest_energy_contcar(round_number));
}

std::string Round::job_dir(std::size_t nth_posistion_to_check, std::size_t round_number)
{
	std::string dir;

	if (round_number < 10) 
		dir = "r0" + std::to_string(round_number);
	else 
		dir = "r" + std::to_string(round_number);

	if (nth_posistion_to_check < 10) 
	{
		dir += "-p0" + std::to_string(nth_posistion_to_check);
		return dir + "/";
	} 
	else 
	{
		dir += "-p" + std::to_string(nth_posistion_to_check);
		return dir + "/";
	}
}

void Round::copy_local_vasp_input_files(std::string job_dir)
{
	std::string incar_in = "INCAR";
	std::string pocar_in = "POTCAR";
	std::string kpoin_in = "KPOINTS";
	std::string incar_ot = job_dir + "INCAR";
	std::string pocar_ot = job_dir + "POTCAR";
	std::string kpoin_ot = job_dir + "KPOINTS";
	
	std::ifstream incar_ifs(incar_in, std::ios::binary);
	std::ofstream incar_ofs(incar_ot, std::ios::binary);
	std::ifstream pocar_ifs(pocar_in, std::ios::binary);
	std::ofstream pocar_ofs(pocar_ot, std::ios::binary);
	std::ifstream kpoin_ifs(kpoin_in, std::ios::binary);
	std::ofstream kpoin_ofs(kpoin_ot, std::ios::binary);

	incar_ofs << incar_ifs.rdbuf();
	pocar_ofs << pocar_ifs.rdbuf();
	kpoin_ofs << kpoin_ifs.rdbuf();

}

std::vector<std::string> Round::all_possible_dirs(std::size_t round_number)
{
	std::vector<std::string> possible_dirs;

	for (std::size_t i = 0; i < m_total_intercalants; ++i) 
		possible_dirs.push_back(job_dir(i, round_number));

	return possible_dirs;
}

std::vector<std::string> Round::all_valid_dirs(std::size_t round_number)
{
	std::vector<std::string> valid_dirs;

	if (round_number == 0) 
	{
		for (auto i : all_possible_dirs(round_number)) valid_dirs.push_back(i);
	} 
	else 
	{
		for (auto i : all_possible_dirs(round_number)) 
		{
			if (boost::filesystem::exists(i)) valid_dirs.push_back(i);
		}
	}

	if (valid_dirs.size() == 0) {
		std::cout << "no valid dirs, probably all jobs not converged\n";
		exit(-1);
	} 
	
	return valid_dirs;
}

std::vector<std::size_t> Round::all_valid_check_positions(std::size_t round_number)
{
	std::vector<std::size_t> check_positions;
	std::vector<std::string> valid_dirs = all_valid_dirs(round_number);
	std::string index;

	for (auto dir : valid_dirs)
	{
		index = dir.substr(5,2);
		check_positions.push_back(static_cast<std::size_t>(std::stoi(index)));
	}

	return check_positions;
}



///////////////////////////////////////////////////////////////////////////////
//                                                                           //
//                          *** Poscar.cpp ***                               //
//                                                                           //
// Base class responsible for reading in and storing the information         //
// contained within the VASP output file POSCAR                              //
//                                                                           //
// created November 20, 2018                                                 //
// copyright Christopher N. Singh Binghamton University Physics              //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////

#include "volpro/Poscar.hpp"
#include <cassert>

void Poscar::initialize(std::string filename)
{
	is_initialized = true;
	initialization_file = filename;

	std::ifstream file(filename.c_str());

	if (file.is_open()) {
		// Working variable, stringstream of each line in file 
		std::istringstream streamline;

		// Pull the header out of the file	
		std::getline(file, header);

		// Pull scale factor out and put it in the class member
		get_streamline(file, streamline);
		streamline >> scale_factor;

		// Pull a_lattice_vector
		get_streamline(file, streamline);
		streamline >> a_lattice_vector.x;
		streamline >> a_lattice_vector.y;
		streamline >> a_lattice_vector.z;
		
		// Pull b_lattice_vector
		get_streamline(file, streamline);
		streamline >> b_lattice_vector.x;
		streamline >> b_lattice_vector.y;
		streamline >> b_lattice_vector.z;
		
		// Pull c_lattice_vector
		get_streamline(file, streamline);
		streamline >> c_lattice_vector.x;
		streamline >> c_lattice_vector.y;
		streamline >> c_lattice_vector.z;

		std::string element_string;
		std::size_t element_num;

		// Pull line with element names and push into a vector
		elements.clear();
		get_streamline(file, streamline);
		while(streamline >> element_string) elements.push_back(element_string);
		
		// Pull line with element quantities and push into a vector
		quantities.clear();
		get_streamline(file, streamline);
		while(streamline >> element_num) quantities.push_back(element_num);

		// Pull out the type of coordinate system
		std::getline(file, coordinate_system);

		int n = std::accumulate(quantities.begin(), quantities.end(), 0);

		Point p;
		positions.clear();
		for (int pos = 0; pos < n; ++pos) {
			get_streamline(file, streamline);
			streamline >> p.x;
			streamline >> p.y;
			streamline >> p.z;
			positions.push_back(p);
		}
	} else {
		open_ascii_escape("red");
		std::cout << "\n ERROR!!! POSCAR NOT FOUND\n\n";
		close_ascii_escape();
		std::exit(-1);
	}
}

void Poscar::reset()
{
	initialize(initialization_file);
}

void Poscar::remove_atom(std::string element, std::size_t index)
{
	if (is_initialized) {

		if (element_exists(element) && has_valid_index(element, index)) {

			erase_position(element, index);
			decrement_quantity(element);

			if (quantities[element_index(element)] == 0) {
				open_ascii_escape("yellow");
				std::cout << "\n WARNING!!! ERASED ATOM TYPE COMPLETELY - POTCAR\n\n";
				close_ascii_escape();
				quantities.erase(quantities.begin() + static_cast<long>(element_index(element)));
				elements.erase(elements.begin() + static_cast<long>(element_index(element)));
			}

		} else {
			open_ascii_escape("red");
			std::cout << "\n ERROR!!! ";	
			std::cout << "YOU TRIED TO DELETE AT ATOM THAT DOESN'T EXIST\n\n";	
			close_ascii_escape();
			std::exit(-1);
		}
	
	} else {
		open_ascii_escape("red");
		std::cout << "\n ERROR!!! MUST INITIALIZE POSCAR\n\n";
		close_ascii_escape();
		std::exit(-1);
	}
}

void Poscar::insert_atom(std::string element, const Point &p)
{
	if (is_initialized) {
		if (element_exists(element)) {
			insert_position(element, p);
			increment_quantity(element);
		} else {
			open_ascii_escape("yellow");
			std::cout << "\n WARNING!!! ADDED NEW ELEMENT TO SYSTEM - POTCAR\n\n";
			close_ascii_escape();
			elements.push_back(element);
			quantities.push_back(1);
			positions.push_back(p);
		}
	} else {
		open_ascii_escape("red");
		std::cout << "\n ERROR!!! MUST INITIALIZE POSCAR\n\n";
		std::exit(-1);
	}
}

void Poscar::write(std::string filename) const
{
	if (is_initialized) {
		std::ofstream file(filename.c_str());

		file << header << "\n";
		file << std::fixed;
		file << std::setprecision(1) << scale_factor << "\n";
		file << std::setprecision(10);

		file << "        ";
		file << a_lattice_vector.x << "         ";
		file << a_lattice_vector.y << "         ";
		file << a_lattice_vector.z << "\n";

		file << "        ";
		file << b_lattice_vector.x << "         ";
		file << b_lattice_vector.y << "         ";
		file << b_lattice_vector.z << "\n";
		
		file << "        ";
		file << c_lattice_vector.x << "         ";
		file << c_lattice_vector.y << "         ";
		file << c_lattice_vector.z << "\n";

		file << "    ";
		for (std::size_t i = 0; i < elements.size(); ++i) 
			file << elements[i] << "    ";
		file << "\n";

		file << "    ";
		for (std::size_t i = 0; i < quantities.size(); ++i) 
			file << quantities[i] << "    ";
		file << "\n";

		file << coordinate_system << "\n";

		file << std::setprecision(9);
		for (std::size_t i = 0; i < positions.size(); ++i) {
			file << "     ";
			file << (positions[i].x == 0.0 ? std::abs(positions[i].x) : positions[i].x) << "         ";
			file << (positions[i].y == 0.0 ? std::abs(positions[i].y) : positions[i].y) << "         ";
			file << (positions[i].z == 0.0 ? std::abs(positions[i].z) : positions[i].z) << "\n";
		}
	} else {
		open_ascii_escape("red");
		std::cout << "\n ERROR!!! MUST INITIALIZE POSCAR\n\n";
		close_ascii_escape();
		std::exit(-1);
	}
}

void Poscar::print(void) const 
{
	if (is_initialized) {
		std::cout << header << "\n";
		std::cout << std::fixed;
		std::cout << std::setprecision(1) << scale_factor << "\n";
		std::cout << std::setprecision(10);

		std::cout << "        ";
		std::cout << a_lattice_vector.x << "         ";
		std::cout << a_lattice_vector.y << "         ";
		std::cout << a_lattice_vector.z << "\n";

		std::cout << "        ";
		std::cout << b_lattice_vector.x << "         ";
		std::cout << b_lattice_vector.y << "         ";
		std::cout << b_lattice_vector.z << "\n";
		
		std::cout << "        ";
		std::cout << c_lattice_vector.x << "         ";
		std::cout << c_lattice_vector.y << "         ";
		std::cout << c_lattice_vector.z << "\n";

		std::cout << std::setw(6);
		for (std::size_t i = 0; i < elements.size(); ++i) 
			std::cout << elements[i] << "\t";
		std::cout << "\n";

		std::cout << std::setw(6);
		for (std::size_t i = 0; i < quantities.size(); ++i) 
			std::cout << quantities[i] << "\t";
		std::cout << "\n";

		std::cout << coordinate_system << "\n";

		std::cout << std::setprecision(9);
		for (std::size_t i = 0; i < positions.size(); ++i) {
			std::cout << "     ";
			std::cout << (positions[i].x == 0.0 ? std::abs(positions[i].x) : positions[i].x) << "         ";
			std::cout << (positions[i].y == 0.0 ? std::abs(positions[i].y) : positions[i].y) << "         ";
			std::cout << (positions[i].z == 0.0 ? std::abs(positions[i].z) : positions[i].z) << "\n";
		}
	} else {
		open_ascii_escape("red");
		std::cout << "\n ERROR!!! MUST INITIALIZE POSCAR\n\n";
		close_ascii_escape();
		std::exit(-1);
	}
}

Point Poscar::get_posistion(std::string element, std::size_t index)
{
	if (is_initialized) 
	{
		if (element_exists(element) && has_valid_index(element, index)) 
		{
			if (element_index(element) == 0) 
			{
				return positions[element_index(element) + index];
			} 
			else 
			{
				for (std::size_t i = 0; i < element_index(element); ++i) 
				{
					index += quantities[i];
				}
				
				return positions[element_index(element) + index];
			}
		} 
		else 
		{
			open_ascii_escape("red");
			std::cout << "\n ERROR!!! ATOM DOESN'T EXISTS\n\n";	
			close_ascii_escape();
			std::exit(-1);
		}
	} 
	else 
	{
		open_ascii_escape("red");
		std::cout << "\n ERROR!!! MUST INITIALIZE POSCAR\n\n";
		close_ascii_escape();
		std::exit(-1);
	}
}

std::size_t Poscar::get_quantity(std::string element) 
{
	if (is_initialized) 
	{
		if (element_exists(element)) 
		{
			return quantities[element_index(element)];
		} 
		else 
		{
			open_ascii_escape("red");
			std::cout << "\n ERROR!!! ATOM DOESN'T EXISTS\n\n";
			close_ascii_escape();
			std::exit(-1);
		}
	} 
	else 
	{ 
		open_ascii_escape("red");
		std::cout << "\n ERROR!!! MUST INITIALIZE POSCAR\n\n";
		close_ascii_escape();
		std::exit(-1);
	}
}
void Poscar::erase_position(std::string element, std::size_t index)
{
	std::size_t start = index;

	if (element_index(element) != 0) {
		for (std::size_t i = 0; i < element_index(element); ++i) 
			start += quantities[i];
	}

	positions.erase(positions.begin() + static_cast<unsigned>(start));
}

void Poscar::insert_position(std::string element, const Point &p)
{
	int location = 0;

	for (std::size_t i = 0; i <= element_index(element); ++i) 
		location += quantities[i];

	positions.insert(positions.begin() + location, p);
}

void Poscar::decrement_quantity(std::string element)
{
	if (quantities[element_index(element)] <= 0) {
		open_ascii_escape("red");
		std::cout << "\n ERROR!!! cannot decrement atom below zero \n\n";
		close_ascii_escape();
		exit(-1);
	} else {
		quantities[element_index(element)] -= 1;
	}
}

void Poscar::increment_quantity(std::string element)
{
	quantities[element_index(element)] += 1;
}


bool Poscar::element_exists(std::string element)
{
	if (std::find(elements.begin(), elements.end(), element) != elements.end())
		return true;
	else 
		return false;
}

std::size_t Poscar::element_index(std::string element)
{
	std::vector<std::string>::iterator location;
	location = std::find(elements.begin(), elements.end(), element);
	
	if (location != elements.end()) 
		return static_cast<std::size_t> (std::distance(elements.begin(), location));
	else {
		std::cout << "\n Could not determine element index, ";
		std::cout << "this should not have happened \n";
		exit(-1);
	}
}

bool Poscar::has_valid_index(std::string element, std::size_t index)
{
	if (index <= quantities[element_index(element)])
		return true;
	else 
		return false;
}

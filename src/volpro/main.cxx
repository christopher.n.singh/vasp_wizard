///////////////////////////////////////////////////////////////////////////////
//                                                                           //
//                           *** main.cpp ***                                //
//                                                                           //
// created November 20, 2018                                                 //
// copyright Christopher N. Singh Binghamton University Physics              //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////

#include <volpro/Poscar.hpp>
#include <volpro/Parser.hpp>
#include <volpro/Slurm.hpp>
#include <volpro/Round.hpp>
#include <volpro/Outcar.hpp>
#include <nixio/nixio.hpp>
#include <boost/lexical_cast.hpp>

int main()
{
	welcome_message_v2();
	
	Parser docket("DOCKET");

	Slurm batchfile;
		
	batchfile.set_ntasks(docket.value_of_key("ntasks"));
	batchfile.set_partition(docket.value_of_key("partition"));
	batchfile.set_nodes(docket.value_of_key("nodes"));
	batchfile.set_tasks_per_node(docket.value_of_key("tasks_per_node"));
	batchfile.set_cpus_per_task(docket.value_of_key("value_of_key"));
	batchfile.set_sockets_per_node(docket.value_of_key("sockets_per_node"));
	batchfile.set_cores_per_socket(docket.value_of_key("cores_per_socket"));
	batchfile.set_threads_per_core(docket.value_of_key("threads_per_core"));
	batchfile.set_mem_per_cpu(docket.value_of_key("mem_per_cpu"));
	batchfile.set_mem(docket.value_of_key("mem"));
	batchfile.set_exclude(docket.value_of_key("exclude"));
	batchfile.set_user_strings(docket.user_strings());
	batchfile.set_mail_type(docket.value_of_key("mail_type"));
	batchfile.set_vasp_version(docket.value_of_key("vasp_version"));
	
	std::string intercalant, frozen_positions;

	intercalant = docket.value_of_key("intercalant");
	frozen_positions = docket.value_of_key("frozen_positions");
	
	Poscar original_supercell;
	original_supercell.initialize("POSCAR");

	Round round(intercalant, original_supercell);

	std::size_t fp = boost::lexical_cast<std::size_t>(frozen_positions);
	round.generate_posistion_test(fp, batchfile);

	auto script_index = [fp](void) -> std::string
	{
		if (fp < 10) return "0" + std::to_string(fp);
		else return std::to_string(fp);
	};

	std::string subc = "submit_round" + script_index() + ".sh";

	open_ascii_escape("green");
	std::cout << "\n Job dirs successfully generated ->";
	std::cout << " to slurm submit all, execute ./" + subc + "\n\n";
	close_ascii_escape();


	return 0;
}


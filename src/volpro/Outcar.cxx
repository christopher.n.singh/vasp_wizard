///////////////////////////////////////////////////////////////////////////////
//                                                                           //
//                          *** Outcar.cpp ***                               //
//                                                                           //
/// Respoinsible for reading and mangaing data from OUTCAR                   //
//                                                                           //
// created November 24, 2018                                                 //
// copyright Christopher N. Singh Binghamton University Physics              //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////

#include "volpro/Outcar.hpp"

void Outcar::initialize(std::string filename)
{
	m_filename = filename;
	std::ifstream file(filename.c_str());
	std::string line;
	std::istringstream streamline;

	std::string free_nrg("free  energy");
	std::string internal_nrg("energy  without");
	std::string reached_required_accuracy
		("reached required accuracy - stopping structural energy minimisation");
	std::string w;
	bool free_energy_found = false;
	bool internal_energy_found = false;
	bool converged = false;

	if (file.is_open()) {
		while (getline(file, line)) {
			if (line.find(free_nrg) != std::string::npos) {
				form_streamline(line, streamline);
				streamline >> w;
				streamline >> w;
				streamline >> w;
				streamline >> w;
				streamline >> free_energy;
				free_energy_found = true;
			} 

			if (line.find(internal_nrg) != std::string::npos) {
				form_streamline(line, streamline);
				streamline >> w;
				streamline >> w;
				streamline >> w;
				streamline >> internal_energy;
				streamline >> w;
				streamline >> w;
				streamline >> internal_energy;
				internal_energy_found = true;
			}
			
			if (line.find(reached_required_accuracy) != std::string::npos) {
				converged = true;
			}
		}

		if (!free_energy_found || !internal_energy_found || !converged) {
			open_ascii_escape("yellow");
			std::cout << "WARNING!!! SYSTEM NOT CONVERGED\n\n";
			close_ascii_escape();
			//exit(-1);
		}

	} else {
		open_ascii_escape("red");
		std::cout << "\n ERROR!!! COULD NOT FIND OUTCAR\n\n";
		close_ascii_escape();
		exit(-1);
	}
}


bool operator> (const Outcar& o1, const Outcar& o2)
{
	if (o1.free_energy > o2.free_energy)// && o1.internal_energy > o2.internal_energy)
		return true;
	else
		return false;
}

bool operator>=(const Outcar& o1, const Outcar& o2)
{
	if (o1.free_energy >= o2.free_energy)// && o1.internal_energy >= o2.internal_energy)
		return true;
	else
		return false;
}

bool operator< (const Outcar& o1, const Outcar& o2)
{
	if (o1.free_energy < o2.free_energy)// && o1.internal_energy < o2.internal_energy)
		return true;
	else
		return false;
}

bool operator<=(const Outcar& o1, const Outcar& o2)
{
	if (o1.free_energy <= o2.free_energy)// && o1.internal_energy <= o2.internal_energy)
		return true;
	else
		return false;
}







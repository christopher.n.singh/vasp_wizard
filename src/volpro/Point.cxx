///////////////////////////////////////////////////////////////////////////////
//                                                                           //
//                           *** Point.cpp ***                               //
//                                                                           //
//                                                                           //
// created November 24, 2018                                                 //
// copyright Christopher N. Singh Binghamton University Physics              //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////

#include "volpro/Point.hpp"

std::ostream& operator<<(std::ostream& os, const Point& point)
{
	os << "{";
	os << point.x;
	os << ",";
	os << point.y;
	os << ",";
	os << point.z;
	os << "}\n";

	return os;
}

std::istream& operator>>(std::istream& is, Point& point)
{
	is >> point.x;
	is >> point.y;
	is >> point.z;
	return is;
}

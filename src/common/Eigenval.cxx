///////////////////////////////////////////////////////////////////////////////
//                                                                           //
//                          *** Eigenval.cpp ***                             //
//                                                                           //
/// Responsible for managing information within EIGENVAL file                //
//                                                                           //
// created November 09, 2018                                                 //
// copyright Christopher N. Singh Binghamton University Physics              //
//                                                                           //
///////////////////////////////////////////////////////////////////////////////

#include "common/Eigenval.hpp"

void Eigenval::write_bandcar()
{
	bool eigenval_exists = boost::filesystem::exists("EIGENVAL");
	bool outcar_exists = boost::filesystem::exists("OUTCAR");

	double efermi = 0;

	if (outcar_exists)
	{
		std::system("grep E-fermi OUTCAR | awk '{print $3}' > fermi");
		std::ifstream fermi("fermi");

		if (fermi.is_open())
		{
			std::string line;
			std::istringstream streamline;

			get_streamline(fermi, streamline);
			streamline >> efermi;
		}
	}
	else
	{
		open_ascii_escape("yellow");
		std::cout << "\n WARNING: Failed to determine E-fermi, bands unshifted\n\n";
		close_ascii_escape();
	}

	if (eigenval_exists) 
	{
		std::string input_file = "EIGENVAL";
		std::string output_file = "BANDCAR";

		initialize(input_file);

		std::ofstream band_stream(output_file.c_str());

		if (band_stream.is_open()) 
		{
			band_stream << "# " << header << "\n";
			band_stream << "#   k      NANDS*E\n";

			for (int k = 0; k < kpoints; ++k) 
			{
				band_stream << std::setw(5);
				band_stream << k << "\t";

				for (int band = 0; band < bands; ++band) 
				{
					band_stream << std::setw(5);
					band_stream << std::scientific;
					band_stream << energy(k, band)-efermi << "\t";
				}

				band_stream << "\n";
			}
		} 
		else 
		{
			open_ascii_escape("red");
			std::cout << "\n ERROR!!! EIGENVAL NOT FOUND\n\n";
			close_ascii_escape();
			std::exit(-1);
		}
		std::cout << "\n";
		std::cout << " Success!\n";
		std::cout << " Bands data written to " << output_file << "\n";
	}

}

void Eigenval::write_gapcar()
{
	bool file_exists = boost::filesystem::exists("EIGENVAL");

	if (!file_exists) {
		
		open_ascii_escape("red");
		std::cout << "\n ERROR!!! NO EIGENVAL ON FILE\n\n";
		close_ascii_escape();
		exit(-1);

	} else if (file_exists) {
		
		std::string input_file = "EIGENVAL";
		std::string output_file = "GAPCAR";

		initialize(input_file);
		write_momentum_dependent_gap(output_file);

		std::cout << "\n";
		std::cout << " Success!\n";
		std::cout << " Eg(k1, k2) data written to " << output_file << "\n";

		std::cout << " MIN gap in your spectrum: ";
	    std::cout << std::scientific << smallest_gap() << " (eV)\n";

		std::cout << " MAX gap in your spectrum: ";
		std::cout << std::scientific << largest_gap() << " (eV)\n";
		std::cout << "\n";
	}
}
	


void Eigenval::write_gapcar(int argc, char *argv[])
{
	bool file_exists = boost::filesystem::exists("EIGENVAL");

	if (argc == 1 && !file_exists) {
		
		std::cout << "\n";
		std::cout << " Seems like I cannot find the EIGENVALUE file!\n";
		std::cout << " In this case, you can pass the name yourself\n";
		std::cout << "\n";
		std::cout << " USAGE: gap <MY_EIGENVAL_FILE> <OPTIONAL_OUTPUT_NAME>\n";
		std::cout << std::endl;
		exit(-1);

	} else if (argc == 1 && file_exists) {
		
		std::string input_file = "EIGENVAL";
		std::string output_file = "GAPCAR";

		initialize(input_file);
		write_momentum_dependent_gap(output_file);

		std::cout << "\n";
		std::cout << " Success!\n";
		std::cout << " Eg(k1, k2) data written to " << output_file << "\n";

		std::cout << " MIN gap in your spectrum: ";
	    std::cout << std::scientific << smallest_gap() << " (eV)\n";

		std::cout << " MAX gap in your spectrum: ";
		std::cout << std::scientific << largest_gap() << " (eV)\n";
		std::cout << "\n";
	
	} else if (argc == 2) {
	
		std::string input_file = argv[1];
		std::string output_file = "GAPCAR";

		initialize(input_file);
		write_momentum_dependent_gap(output_file);

		std::cout << "\n";
		std::cout << " Success!\n";
		std::cout << " Eg(k1, k2) data written to " << output_file << "\n";

		std::cout << " MIN gap in your spectrum: ";
	    std::cout << std::scientific << smallest_gap() << " (eV)\n";

		std::cout << " MAX gap in your spectrum: ";
		std::cout << std::scientific << largest_gap() << " (eV)\n";
		std::cout << "\n";
	
	} else if (argc == 3) {
		
		std::string input_file = argv[1];
		std::string output_file = argv[2];

		initialize(input_file);
		write_momentum_dependent_gap(output_file);

		std::cout << "\n";
		std::cout << " Success!\n";
		std::cout << " Eg(k1, k2) data written to " << output_file << "\n";

		std::cout << " MIN gap in your spectrum: ";
	    std::cout << std::scientific << smallest_gap() << " (eV)\n";

		std::cout << " MAX gap in your spectrum: ";
		std::cout << std::scientific << largest_gap() << " (eV)\n";
		std::cout << "\n";
		
	} else {
		std::cout << "I am not designed to handle that\n";
		std::cout << " USAGE: vasp_wiz <MY_EIGENVAL_FILE> <OPTIONAL_OUTPUT_NAME>\n";
		exit(-1);
	}
}

void Eigenval::find_largest_gap_k1k2(void)
{
	double largest = gaps[0];
	largest_gap_k1 = 0;
	largest_gap_k2 = 0;

	for (int k1 = 0; k1 < kpoints; ++k1) {
		for (int k2 = 1; k2 < kpoints; ++k2) {
			if (gaps[k1 * kpoints + k2] > largest) {
				largest = gaps[k1 * kpoints + k2];
				largest_gap_k1 = k1;
				largest_gap_k2 = k2;
			}
		}
	}
}

void Eigenval::find_smallest_gap_k1k2(void)
{
	double smallest = gaps[0];
	smallest_gap_k1 = 0;
	smallest_gap_k2 = 0;

	for (int k1 = 0; k1 < kpoints; ++k1) {
		for (int k2 = 1; k2 < kpoints; ++k2) {
			if (gaps[k1 * kpoints + k2] < smallest) {
				smallest = gaps[k1 * kpoints + k2];
				smallest_gap_k1 = k1;
				smallest_gap_k2 = k2;
			}
		}
	}
}

double Eigenval::largest_gap(void)
{
	return *std::max_element(gaps.begin(), gaps.end());
}

double Eigenval::smallest_gap(void)
{
	return *std::min_element(gaps.begin(), gaps.end());
}

void Eigenval::write_momentum_dependent_gap(std::string file)
{
	std::ofstream gap_data_stream(file.c_str());

	if (gap_data_stream.is_open()) {

		gap_data_stream << "# " << header << "\n";
		gap_data_stream << "#   k1      k2       Eg (eV)\n";

		for (int k1 = 0; k1 < kpoints; ++k1) {
			for (int k2 = 0; k2 < kpoints; ++k2) {
				gap_data_stream << std::setw(5);
				gap_data_stream << k1 << "\t";
				gap_data_stream << std::setw(5);
				gap_data_stream << k2 << "\t";
				gap_data_stream << std::setw(15);
				gap_data_stream << std::scientific;
				gap_data_stream << gaps[k1 * kpoints + k2];
				gap_data_stream << "\n";
			}
		}
	} else {
		open_ascii_escape("red");
		std::cout << "\n ERROR!!! EIGENVAL NOT FOUND\n\n";
		close_ascii_escape();
		std::exit(-1);
	}
}

void Eigenval::calculate_momentum_dependent_gap(void)
{
	//gaps = new double[kpoints*kpoints];
	gaps.resize(kpoints*kpoints);

	for (int k1 = 0; k1 < kpoints; ++k1) {
		for (int k2 = 0; k2 < kpoints; ++k2) 
			gaps[k1 * kpoints + k2] = gap(k1, k2);
	}
}

double Eigenval::gap(int k1, int k2) 
{
	return conduction_band_minimum(k1) - valence_band_maximum(k2);
}

double Eigenval::energy(int kpoint, int band)
{
	return value[band*3 + 1 + kpoint*bands*3];
}

double Eigenval::occupation(int kpoint, int band)
{
	return value[band*3 + 2 + kpoint*bands*3];	
}

double Eigenval::conduction_band_minimum(int kpoint)
{
	for (int band = 0; band < bands; ++band) 
		if (occupation(kpoint, band) == 0.0) return energy(kpoint, band);

	std::cout << " Count not find the bottom of the conduction band!\n";
	exit(-100);
}

double Eigenval::valence_band_maximum(int kpoint)
{
	for (int band = 0; band < bands; ++band) 
		if (occupation(kpoint, band) == 0.0) return energy(kpoint, band - 1);

	std::cout << " Count not find the top of the valence band!\n";
	exit(-200);
}

void Eigenval::initialize(std::string filename)
{
	std::ifstream file(filename.c_str());

	if (file.is_open()) {
		std::string line;
		std::istringstream streamline;

		get_streamline(file, streamline);
		streamline >> ions;
		streamline >> ions;
		streamline >> pair_correlation_loops;
		streamline >> ispin;

		get_streamline(file, streamline);
		streamline >> volume_of_cell;
		streamline >> a_lattice_param;
		streamline >> b_lattice_param;
		streamline >> c_lattice_param;
		streamline >> yet_unknown_parameter;
		
		get_streamline(file, streamline);
		streamline >> temperature;

		std::getline(file, car);
		std::getline(file, header);
		
		get_streamline(file, streamline);
		streamline >> electrons;
		streamline >> kpoints;
		streamline >> bands;

		std::getline(file, line);

		kvecs.resize(kpoints*4);
		value.resize(kpoints*bands*3);

		for (int kpoint = 0; kpoint < kpoints; ++kpoint) {
			get_streamline(file, streamline);
			for (int i = 0; i < 4; ++i) streamline >> kvecs[kpoint * 4 + i];
			for (int band = 0; band < bands; ++band) {
				get_streamline(file, streamline);
				for (int i = 0; i < 3; ++i) {
					streamline >> value[band*3 + i + kpoint*bands*3];
				}
			}
			std::getline(file, line);
		}

		calculate_momentum_dependent_gap();
		find_largest_gap_k1k2();
		find_smallest_gap_k1k2();

	} else {
		open_ascii_escape("red");
		std::cout << "\n ERROR!!! EIGENVAL NOT FOUND\n\n";
		close_ascii_escape();
		std::exit(-1);
	}
}

void Eigenval::print(void) 
{
	std::cout << ions << " " << ions << " ";
	std::cout << pair_correlation_loops << " ";
	std::cout << ispin << "\n";

	std::cout << volume_of_cell << " ";
	std::cout << a_lattice_param << " ";
	std::cout << b_lattice_param << " ";
	std::cout << c_lattice_param << " ";
	std::cout << yet_unknown_parameter << "\n";

	std::cout << temperature << "\n";

	std::cout << car << "\n";
	std::cout << header << "\n";

	std::cout << electrons << " ";
	std::cout << kpoints << " ";
	std::cout << bands << "\n";

	std::cout << std::setw(10);
	for (int kpoint = 0; kpoint < kpoints; ++kpoint) {
		for (int i = 0; i < 4; ++i) {
			std::cout << kvecs[kpoint * 4 + i] << "   ";
		}
		std::cout << "\n";
		for (int band = 0; band < bands; ++band) {
			for (int j = 0; j < 3; ++j) {
				std::cout << std::setw(10);
				std::cout << value[band * 3 + j + kpoint*bands*3] << "  ";
			}
			std::cout << "\n";
		}
	}

}
